package org.macno.puma.activity;

import static org.macno.puma.PumaApplication.APP_NAME;
import static org.macno.puma.PumaApplication.DEBUG;
import static org.macno.puma.PumaApplication.K_PUMA_SETTINGS;
import static org.macno.puma.activity.HomeActivity.ACTION_ACTIVITY_POSTED;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.macno.puma.R;
import org.macno.puma.core.Account;
import org.macno.puma.manager.AccountManager;
import org.macno.puma.provider.Pumpio;
import org.macno.puma.util.LocationUtil;
import org.markdown4j.Markdown4jProcessor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class ComposeActivity extends Activity {

	public static final int ACTION_REPLY = 1;

	public static final String EXTRA_ACCOUNT_UUID = "extraAccountUUID";
	public static final String EXTRA_ACTIVITY = "extraActivity";
	public static final String EXTRA_ACTION = "extraAction";

	private static final String K_GEO_ENABLED = "geoEnabled";
	private static final String K_PUBLIC_NOTE = "publicNote";

	private static final String K_GEO_ENABLED_GLOBALLY = "geoEnabledGlobally";

	private final int CHOOSE_FILE_ID = 0;

	private Account mAccount;
	private ArrayList<Account> mAccounts;

	private NotificationManager mNotificationManager;
	private SharedPreferences mSettings;

	private Context mContext;

	private EditText mTitle;
	private EditText mNote;
	private CheckBox mCheckBoxLocation;
	private CheckBox mCheckBoxPublic;

	private PostHandler mHandler = new PostHandler(this);

	private boolean mPreserveAccount = false;
	private String mActivity;
	private int mAction = 0;

	private Bitmap mBitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.activity_compose);
		Bundle extras = getIntent().getExtras();

		// Get details of Share content by Intent
		Intent intent = getIntent();
		String action = intent.getAction();
		String type = intent.getType();

		String accountUUID = "";

		boolean multiAccount = true;

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey(EXTRA_ACCOUNT_UUID)) {
				accountUUID = savedInstanceState.getString(EXTRA_ACCOUNT_UUID);
				mPreserveAccount = true;
			}
			if (savedInstanceState.containsKey(EXTRA_ACTIVITY)) {
				mActivity = savedInstanceState.getString(EXTRA_ACTIVITY);
				mAction = savedInstanceState.getInt(EXTRA_ACTION);
				multiAccount = false;
			}
		} else if (extras != null) {
			if (extras.containsKey(EXTRA_ACCOUNT_UUID)) {
				accountUUID = extras.getString(EXTRA_ACCOUNT_UUID);
				mPreserveAccount = true;
			}
			if (extras.containsKey(EXTRA_ACTIVITY)) {
				mActivity = extras.getString(EXTRA_ACTIVITY);
				mAction = extras.getInt(EXTRA_ACTION);
				multiAccount = false;
			}
		}

		AccountManager am = new AccountManager(this);
		if ("".equals(accountUUID)) {
			mAccount = am.getDefaultAccount();
		} else {
			mAccount = am.getAccount(accountUUID);
		}

		if (mAccount == null) {
			AccountAddActivity.startActivity(this);
			finish();
		}

		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mSettings = getSharedPreferences(K_PUMA_SETTINGS, Context.MODE_PRIVATE);

		mTitle = (EditText) findViewById(R.id.title);
		mNote = (EditText) findViewById(R.id.note);

		mCheckBoxLocation = (CheckBox) findViewById(R.id.enable_location);
		handleGeoCheckbox();

		mCheckBoxPublic = (CheckBox) findViewById(R.id.public_post);
		handlePublicCheckbox();

		
		Button btn_remove_attachment = (Button)findViewById(R.id.btn_remove_attachment);
		btn_remove_attachment.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				doShowConfirmRemoveAttachment();
			}
		});
		if (multiAccount) {
			mAccounts = am.getAccounts();
			if (mAccounts.size() == 1) {
				multiAccount = false;
			}
		}

		if (multiAccount) {
			setAccountsSpinner();
		} else {
			hideSpinner();
		}

		mNotificationManager.cancel(0);

		if (mAction == ACTION_REPLY) {
			findViewById(R.id.ll_title).setVisibility(View.GONE);

			/*
			 * TODO: why hide the location checkbox?! Hiding means that comments
			 * can submit the user's location without the user knowing (if
			 * enabled at the last post, the preference is kept) ! Just hide the
			 * Public Note option for comments, as they should be non-public and
			 * the server should decide/protect their visibility.
			 */
			// findViewById(R.id.ll_options).setVisibility(View.GONE);
			findViewById(R.id.public_post).setVisibility(View.GONE);
		}
		if (Intent.ACTION_SEND.equals(action) && type != null) {
			if (type.equals("text/plain")) {
				handleSendText(intent); // Handle text, link etc.
			} else if (type.startsWith("image/")) {
				handleSendImage(intent);
			}
			// add else if for other types (images etc.)
		}
	}

	private void doShowConfirmRemoveAttachment() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.headsup)
				.setMessage(R.string.remove_attachment)
				.setCancelable(true)
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface xdialog,
									int id) {
								findViewById(R.id.ll_attachment).setVisibility(View.GONE);
								mBitmap = null;
								ImageView iv = (ImageView) findViewById(R.id.iv_attachment);
								iv.setImageBitmap(null);
							}
						}).setNegativeButton(android.R.string.no, null)
				.create().show();
			
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(EXTRA_ACTIVITY, mActivity);
		outState.putInt(EXTRA_ACTION, mAction);
		if (mPreserveAccount) {
			outState.putString(EXTRA_ACCOUNT_UUID, mAccount.getUuid());
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.compose, menu);
		if (mAction == ACTION_REPLY) {
			menu.removeItem(R.id.action_add_picture);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_post:
			onPostAction();
			return true;

		case R.id.action_add_picture:
			showFileChooser();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void showFileChooser() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		startActivityForResult(intent, CHOOSE_FILE_ID);
	}

	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		if (requestCode == CHOOSE_FILE_ID) {
			if (resultCode == RESULT_OK) {
				Uri uri = intent.getData();
				if (uri != null) {
					setFileFromUri(uri);
				}
			}

		}

	}

	private void showAttachment() {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				ImageView iv = (ImageView) findViewById(R.id.iv_attachment);
				int w = iv.getWidth();
				if(DEBUG) {
					Log.v(APP_NAME,"ImageView w: " + w);
				}
				if(w > 0 && w < mBitmap.getWidth()) {
					 
					float scale = ((float)w)/ mBitmap.getWidth();
				    
				    
				    if (DEBUG) {
						Log.d(APP_NAME, "scale: " + scale);
				    }
				    // CREATE A MATRIX FOR THE MANIPULATION
				    Matrix matrix = new Matrix();
				    
				    // RESIZE THE BIT MAP
				    matrix.postScale(scale, scale);

				    // "RECREATE" THE NEW BITMAP
				    Bitmap resizedBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, false);
				    
				    iv.setImageBitmap(resizedBitmap);
				} else {
					iv.setImageBitmap(mBitmap);
				}
			}
		};
		mHandler.postDelayed(runnable, 200);
	}
	private void setFileFromUri(Uri uri) {

		if (DEBUG)
			Log.d(APP_NAME, "Uri: " + uri.toString());

		try {

			findViewById(R.id.ll_attachment).setVisibility(View.VISIBLE);
			
			InputStream stream = getContentResolver().openInputStream(uri);
			mBitmap = BitmapFactory.decodeStream(stream);
			try {
				stream.close();
			} catch (IOException e) {
				//
			}
			showAttachment();
			
			
		} catch (FileNotFoundException e) {
			findViewById(R.id.ll_attachment).setVisibility(View.GONE);
			Toast.makeText(mContext,
					"There's a problem accessing this image...",
					Toast.LENGTH_SHORT).show();
		}

	}

	void handleSendText(Intent intent) {
		String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
		if (sharedText != null) {
			Log.d(APP_NAME, "Shared text: " + sharedText);
			// Update UI to reflect text being shared
			mNote = (EditText) findViewById(R.id.note);
			mNote.setText(sharedText);
		}
	}

	void handleSendImage(Intent intent) {
		Uri uri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
		if (uri != null) {
			setFileFromUri(uri);
		}
	}

	private void postComplete() {
		broadcastIntentReload();
	}

	private void postFailed() {
		Toast.makeText(this, R.string.post_failed, Toast.LENGTH_SHORT).show();
	}

	private void insertMarkdownHelper(String startText, String endText,
			boolean newline, boolean spaceAtStart) {
		mNote = (EditText) findViewById(R.id.note);
		Editable noteText = mNote.getText();
		int cursorPos = mNote.getSelectionStart();

		CharSequence textBefore = noteText.subSequence(0, cursorPos);
		CharSequence textAfter = noteText.subSequence(cursorPos,
				noteText.length());

		// start a new line if needed
		if (newline == true && cursorPos > 0) {
			textBefore = textBefore + "\n";
			cursorPos += 1;
		}
		// Strings from i18n chomp trailing spaces
		if (spaceAtStart == true) {
			startText = startText + " ";
		}

		mNote.setText(textBefore + startText + endText + textAfter);
		mNote.setSelection(cursorPos + startText.length());
	}

	public void markdownHelperBold(View v) {
		insertMarkdownHelper(getString(R.string.compose_bold_start),
				getString(R.string.compose_bold_finish), false, false);
	}

	public void markdownHelperItalic(View v) {
		insertMarkdownHelper(getString(R.string.compose_italic_start),
				getString(R.string.compose_italic_finish), false, false);
	}

	public void markdownHelperBoldItalic(View v) {
		insertMarkdownHelper(getString(R.string.compose_bolditalic_start),
				getString(R.string.compose_bolditalic_finish), false, false);
	}

	public void markdownHelperLink(View v) {
		insertMarkdownHelper(getString(R.string.compose_link_start),
				getString(R.string.compose_link_finish), false, false);
	}

	public void markdownHelperImage(View v) {
		insertMarkdownHelper(getString(R.string.compose_image_start),
				getString(R.string.compose_image_finish), false, false);
	}

	public void markdownHelperH1(View v) {
		insertMarkdownHelper(getString(R.string.compose_h1_start),
				getString(R.string.compose_h1_finish), true, true);
	}

	public void markdownHelperH2(View v) {
		insertMarkdownHelper(getString(R.string.compose_h2_start),
				getString(R.string.compose_h2_finish), true, true);
	}

	public void markdownHelperH3(View v) {
		insertMarkdownHelper(getString(R.string.compose_h3_start),
				getString(R.string.compose_h3_finish), true, true);
	}

	public void markdownHelperList(View v) {
		insertMarkdownHelper(getString(R.string.compose_list_start),
				getString(R.string.compose_list_finish), true, true);
	}

	public void markdownHelperQuote(View v) {
		insertMarkdownHelper(getString(R.string.compose_quote_start),
				getString(R.string.compose_quote_finish), true, true);
	}

	public void markdownHelperCode(View v) {
		insertMarkdownHelper(getString(R.string.compose_code_start),
				getString(R.string.compose_code_finish), false, false);
	}

	public void markdownHelperRule(View v) {
		insertMarkdownHelper(getString(R.string.compose_rule_start),
				getString(R.string.compose_rule_finish), true, false);
	}

	private static class PostHandler extends Handler {

		private final WeakReference<ComposeActivity> mTarget;

		private static final int MSG_POST_OK = 0;
		private static final int MSG_POST_KO = 1;

		PostHandler(ComposeActivity target) {
			mTarget = new WeakReference<ComposeActivity>(target);
		}

		public void handleMessage(Message msg) {
			ComposeActivity target = mTarget.get();

			switch (msg.what) {

			case MSG_POST_OK:
				target.postComplete();
				break;

			case MSG_POST_KO:
				target.postFailed();
				break;
			}
		}

		void sendPostComplete(int notificationId) {
			sendEmptyMessage(MSG_POST_OK);
			Log.d(APP_NAME, "Success -- Cancelling notification with ID "
					+ notificationId);
			mTarget.get().mNotificationManager.cancel(notificationId);
		}

		void sendPostFailed(int notificationId) {
			sendEmptyMessage(MSG_POST_KO);
			Log.d(APP_NAME, "Failure -- Cancelling notification with ID "
					+ notificationId);
			mTarget.get().mNotificationManager.cancel(notificationId);
		}
	}

	private void hideSpinner() {
		findViewById(R.id.ll_spinner).setVisibility(View.GONE);
	}

	private void setAccountsSpinner() {
		Spinner spinner = (Spinner) findViewById(R.id.account_spinner);

		ArrayList<CharSequence> accounts = new ArrayList<CharSequence>();
		accounts.add(mAccount.getUsername() + "@" + mAccount.getNode());
		for (Account account : mAccounts) {
			if (account.getUuid().equals(mAccount.getUuid())) {
				continue;
			}
			accounts.add(account.getUsername() + "@" + account.getNode());
		}
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
				this, android.R.layout.simple_spinner_item, accounts);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
	}

	private void handleGeoCheckbox() {

		mCheckBoxLocation = (CheckBox) findViewById(R.id.enable_location);
		boolean geoEnabled = mSettings.getBoolean(K_GEO_ENABLED_GLOBALLY, true);
		if (geoEnabled) {

			if (mSettings.getBoolean(K_GEO_ENABLED, false))
				mCheckBoxLocation.setChecked(true);
			mCheckBoxLocation
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							mSettings.edit()
									.putBoolean(K_GEO_ENABLED, isChecked)
									.commit();
						}

					});
		} else {
			mCheckBoxLocation.setChecked(false);
			mCheckBoxLocation.setVisibility(View.GONE);
		}
	}

	private void handlePublicCheckbox() {

		if (mSettings.getBoolean(K_PUBLIC_NOTE, false))
			mCheckBoxPublic.setChecked(true);
		mCheckBoxPublic
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						mSettings.edit().putBoolean(K_PUBLIC_NOTE, isChecked)
								.commit();
					}

				});

	}

	private void onPostAction() {
		Spinner spinner = (Spinner) findViewById(R.id.account_spinner);
		Account current = mAccount;
		if (spinner.isShown()) {
			Object o = spinner.getSelectedItem();
			// Log.d(APP_NAME,"Spinner obj: " + o);
			current = findAccountByName((String) o);
		}

		String title = mTitle.getText().toString();
		if (title.length() == 0) {
			title = null;
		}

		String note = mNote.getText().toString();
		try {
			note = new Markdown4jProcessor().process(note);
		} catch (IOException e) {
			Log.e(APP_NAME, e.getMessage(), e);
		}

		Location location = null;
		if (mCheckBoxLocation.isChecked()) {
			LocationManager locationManager = (LocationManager) this
					.getSystemService(Context.LOCATION_SERVICE);
			location = LocationUtil
					.getMostRecentLastKnownLocation(locationManager);
		}
		JSONObject inReplyTo = null;
		switch (mAction) {
		case ACTION_REPLY:
			try {
				inReplyTo = new JSONObject(mActivity);
			} catch (JSONException e) {
				Log.e(APP_NAME, e.getMessage(), e);
			}
			break;

		default:
			//
		}

		post(current, title, note, inReplyTo, mCheckBoxPublic.isChecked(),
				location);

	}

	private void post(final Account account, final String title,
			final String note, final JSONObject inReplyTo,
			final boolean isPublicNote, final Location location) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Log.d(APP_NAME, "Creating notification...");
				int notificationId = createNotification(note);
				Log.d(APP_NAME, "Notification " + notificationId
						+ " sent! Continuing to post.");

				Pumpio pumpio = new Pumpio(mContext);
				pumpio.setAccount(account);
				boolean posted = false;

				if (mBitmap != null) {

					try {

						ByteArrayOutputStream out = new ByteArrayOutputStream(
								10240);
						int width = mBitmap.getWidth();
					    int height = mBitmap.getHeight();
					    
					    if (DEBUG) {
							Log.d(APP_NAME, "imageHeight: " + height);
							Log.d(APP_NAME, "imageWidth: " + width);
							
						}
						if (width > 1024 || height > 1024) {
							
						    float scale = 1;
						    if(width > height) {
						    	scale = ((float)1024)/width;
						    } else {
						    	scale = ((float)1024)/height;
						    }
						    
						    if (DEBUG) {
								Log.d(APP_NAME, "scale: " + scale);
						    }
						    // CREATE A MATRIX FOR THE MANIPULATION
						    Matrix matrix = new Matrix();
						    
						    // RESIZE THE BIT MAP
						    matrix.postScale(scale, scale);

						    // "RECREATE" THE NEW BITMAP
						    Bitmap resizedBitmap = Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false);
						    
						    
						    resizedBitmap.compress(CompressFormat.JPEG, 100, out);
						} else {
							mBitmap.compress(CompressFormat.JPEG, 100, out);
						}
						

						posted = pumpio.postImage(title, note, isPublicNote,
								location, "image/jpeg", out.toByteArray());
					} catch (Exception e) {
						Log.e(APP_NAME,e.getMessage(),e);
						mHandler.sendPostFailed(notificationId);
					}
				} else {
					try {
						posted = pumpio.postNote(inReplyTo, title, note,
								isPublicNote, location);
					} catch (Exception e) {
						Log.e(APP_NAME,e.getMessage(),e);
						mHandler.sendPostFailed(notificationId);
					}
				}
				if(posted) {
					mHandler.sendPostComplete(notificationId);
				} else {
					mHandler.sendPostFailed(notificationId);
				}
			}

			private int createNotification(final String note) {
				int latestNotificationId = mSettings.getInt(
						"latestNotificationId", 0);
				latestNotificationId = latestNotificationId + 1;
				mSettings.edit()
						.putInt("latestNotificationId", latestNotificationId)
						.commit();
				Builder notification = new NotificationCompat.Builder(
						getApplicationContext())
						.setSmallIcon(R.drawable.puma_logo)
						.setContentTitle("Posting Message...")
						.setContentText(note);

				Log.d(APP_NAME, "Sending notification...");
				mNotificationManager.notify(latestNotificationId,
						notification.build());
				return latestNotificationId;
			}
		};
		new Thread(runnable).start();
		finish();
	}

	private Account findAccountByName(String name) {
		for (Account account : mAccounts) {
			if (name.equals(account.getUsername() + "@" + account.getNode())) {
				return account;
			}
		}
		return null;
	}

	public static void startActivity(Context context, Account account) {
		Intent homeIntent = new Intent(context, ComposeActivity.class);
		homeIntent.putExtra(HomeActivity.EXTRA_ACCOUNT_UUID, account.getUuid());
		context.startActivity(homeIntent);
	}

	public static void startActivity(Context context, Account account,
			String activityId, int action) {
		Intent homeIntent = new Intent(context, ComposeActivity.class);
		homeIntent.putExtra(ComposeActivity.EXTRA_ACCOUNT_UUID,
				account.getUuid());
		homeIntent.putExtra(ComposeActivity.EXTRA_ACTIVITY, activityId);
		homeIntent.putExtra(ComposeActivity.EXTRA_ACTION, action);

		context.startActivity(homeIntent);
	}

	public static void startActivity(Context context) {
		Intent homeIntent = new Intent(context, ComposeActivity.class);
		context.startActivity(homeIntent);
	}

	private void broadcastIntentReload() {
		Intent intent = new Intent(ACTION_ACTIVITY_POSTED);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	@Override
	public void onBackPressed() {
		if (mTitle.getText().toString().trim().equals("")
				&& mNote.getText().toString().trim().equals("")) {
			super.onBackPressed();
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.headsup)
					.setMessage(R.string.pending_changes)
					.setCancelable(true)
					.setPositiveButton(android.R.string.yes,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface xdialog,
										int id) {
									finish();
								}
							}).setNegativeButton(android.R.string.no, null)
					.create().show();
		}

	}
}
