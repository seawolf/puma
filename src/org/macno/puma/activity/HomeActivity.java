package org.macno.puma.activity;

import static org.macno.puma.PumaApplication.APP_NAME;
import static org.macno.puma.PumaApplication.DEBUG;

import java.util.ArrayList;

import org.macno.puma.R;
import org.macno.puma.adapter.StreamAdapter;
import org.macno.puma.core.Account;
import org.macno.puma.core.Stream;
import org.macno.puma.fragment.StreamFragment;
import org.macno.puma.manager.AccountManager;
import org.macno.puma.manager.StreamManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class HomeActivity extends Activity {

	public static final String ACTION_ACTIVITY_DELETED = "org.macno.puma.ActivityDeleted";
	public static final String ACTION_ACTIVITY_POSTED = "org.macno.puma.ActivityPosted";

	public static final String EXTRA_ACCOUNT_UUID = "extraAccountUUID";
	public static final String EXTRA_STREAM_POS = "extraStreamID";
	
	private static final int K_SETTINGS_RESULT = 10;
	
	private Account mAccount;

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;

	private ArrayList<Stream> mStreamNames;
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminate(true);
		setProgressBarIndeterminateVisibility(false);

		setContentView(R.layout.activity_home);

		Bundle extras = getIntent().getExtras();

		String accountUUID = "";
		if (savedInstanceState != null) {
			accountUUID = savedInstanceState.getString(EXTRA_ACCOUNT_UUID);
		} else if (extras != null) {
			accountUUID = extras.getString(EXTRA_ACCOUNT_UUID);
		}

		AccountManager am = new AccountManager(this);
		mAccount = am.getAccount(accountUUID);

		if (mAccount == null) {
			AccountAddActivity.startActivity(this);
			finish();
		}
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mStreamNames = StreamManager.getStreams();

		mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
                ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(mTitle);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        
//        getActionBar().setHomeButtonEnabled(true);
        
		mDrawerList.setAdapter(new StreamAdapter(this, R.layout.drawer_list_item, mStreamNames));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		
		int streamPos = 0;
		if (savedInstanceState != null) {
			streamPos = savedInstanceState.getInt(EXTRA_STREAM_POS, 0);
		}
		selectItem(streamPos);
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(DEBUG) {
			Log.d(APP_NAME,"onActivityResult: " + requestCode + " res: " + resultCode);
		}

		switch (requestCode) {
		case K_SETTINGS_RESULT:
			if(resultCode == SettingsActivity.K_RESULT_NEED_REFRESH) {
				forceReload();
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void forceReload() {
		selectItem(mDrawerList.getCheckedItemPosition(), true);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private void selectItem(int position) {
		selectItem(position,false);
	}
	
	private void selectItem(int position, boolean force) {
		// Create a new fragment and specify the planet to show based on
		// position
		Stream s = mStreamNames.get(position);

		// Insert the fragment by replacing any existing fragment
		FragmentManager fragmentManager = getFragmentManager();
		
		Fragment currentFragment = fragmentManager.findFragmentByTag(s.id); 
		if(currentFragment != null) {
			if(DEBUG) {
				Log.d(APP_NAME,"Fragment già aperto, esco");
			}
			if(force) {
				fragmentManager.beginTransaction().remove(currentFragment).commit();
				
			} else {
				return;
			}
		}

		Fragment fragment = new StreamFragment();
		Bundle args = new Bundle();
		args.putString(StreamFragment.ARG_STREAM_ID, s.id);
		args.putString(EXTRA_ACCOUNT_UUID, mAccount.getUuid());
		fragment.setArguments(args);

		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment,s.id).commit();
		// Highlight the selected item, update the title, and close the drawer
		mDrawerList.setItemChecked(position, true);
		setTitle(s.name);
		mDrawerLayout.closeDrawer(mDrawerList);
	}
	
	@Override
	public void onResume() {
		super.onResume();

		// Register mMessageReceiver to receive messages.
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.registerReceiver(mMessageReceiver,
				new IntentFilter(ACTION_ACTIVITY_DELETED));
		localBroadcastManager.registerReceiver(mMessageReceiver,
				new IntentFilter(ACTION_ACTIVITY_POSTED));
		
	}

	
	@Override
	protected void onPause() {
		// Unregister since the activity is not visible
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				mMessageReceiver);
		super.onPause();
	}

	// handler for received Intents for the "my-event" event
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();
			if (ACTION_ACTIVITY_DELETED.equals(action)) {
				Toast.makeText(context,
						context.getString(R.string.note_deleted),
						Toast.LENGTH_SHORT).show();
				onClearCacheAction();
			} else if (ACTION_ACTIVITY_POSTED.equals(action)) {
				Toast.makeText(context,
						context.getString(R.string.post_complete),
						Toast.LENGTH_SHORT).show();
				onRefreshAction();
			} else {
				Log.e(APP_NAME, "PumaReceiver with unkown action: " + action);
			}
		}
	};

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(EXTRA_ACCOUNT_UUID, mAccount.getUuid());
		outState.putInt(EXTRA_STREAM_POS, mDrawerList.getCheckedItemPosition());
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {

		case R.id.action_logout:
			onLogoutAction();
			return true;

		case R.id.action_add_account:
			onAddAccountAction();
			return true;

		case R.id.action_compose:
			ComposeActivity.startActivity(this, mAccount);
			return true;

		case R.id.action_refresh:
			onRefreshAction();
			return true;

		case R.id.action_settings:
			openSettings();
			return true;
		case R.id.action_clear_cache:

			onClearCacheAction();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void openSettings() {
		Intent settingsIntent = new Intent(this,SettingsActivity.class);
		startActivityForResult(settingsIntent, K_SETTINGS_RESULT);
	}

	private void onLogoutAction() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.confirm_logout)
				.setCancelable(false)
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface xdialog, int id) {
								doLogout();
							}
						}).setNegativeButton(android.R.string.cancel, null)
				.create().show();
	}

	private void doLogout() {
		AccountManager am = new AccountManager(this);
		am.delete(mAccount);
		MainActivity.startActivity(this);
		finish();
	}

	private void onAddAccountAction() {
		AccountAddActivity.startActivity(this);
		finish();
	}

	private void onClearCacheAction() {
		StreamFragment sf = getCurrentStreamFragment();
		if(sf != null) {
			sf.clearCache();
		}
	}

	private void onRefreshAction() {
		
		StreamFragment sf = getCurrentStreamFragment();
		if(sf != null) {
			sf.reload();
		}
	}

	private StreamFragment getCurrentStreamFragment() {
		int position = mDrawerList.getCheckedItemPosition();
		
		Stream s = mStreamNames.get(position);

		// Insert the fragment by replacing any existing fragment
		FragmentManager fragmentManager = getFragmentManager();
		return (StreamFragment)fragmentManager.findFragmentByTag(s.id);
		
	}
	public static void startActivity(Context context, Account account) {
		Intent homeIntent = new Intent(context, HomeActivity.class);
		homeIntent.putExtra(HomeActivity.EXTRA_ACCOUNT_UUID, account.getUuid());
		context.startActivity(homeIntent);

	}
	
	@Override
	public void onBackPressed() {
		if(mDrawerList.getCheckedItemPosition()==0) {
			super.onBackPressed();
		} else {
			selectItem(0);
		}
	}

}
