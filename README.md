
# Puma


**Puma** is an android client for [pump.io](http://pump.io)

### Version 1.0

* Added GridView layout for large screens (You can disable it from Settings)   
  It uses the cool [StaggeredGridView](https://github.com/etsy/AndroidStaggeredGrid) from Etsy
* Image preview when posting
* Removed ViewPager for a Slide menu